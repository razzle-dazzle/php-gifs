# PHP Gif API

API returning gifs

# Installing App
`composer install`

# Running Tests 
`vendor/bin/phpunit`

# Starting API
`php -S localhost:8000 -t public/`

### Endpoints
API_KEY must be provided in the headers (can be anything).

`v1/gifs/random`

`v1/gifs/search?gif=[gifname]`