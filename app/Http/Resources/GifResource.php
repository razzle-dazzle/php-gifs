<?php

namespace App\Http\Resources;

use Illuminate\Support\Str;
use Illuminate\Http\Resources\Json\JsonResource;

class GifResource
{
    public $title;
    public $slug;

    public function __construct($gif_name){
        $this->title= $gif_name;
        $this->slug= $gif_name;
    }
    
    public function as_resource (){
        return array(
            'data' => array(
                'gif' => array(
                    'title'              => $this->title,
                    'url'                => "https://www.gifapi.com/{$this->slug}.gif")
                )
            );
    }
}