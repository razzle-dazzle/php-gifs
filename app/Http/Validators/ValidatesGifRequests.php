<?php

namespace App\Http\Validators;

use Illuminate\Http\Request;

trait ValidatesGifRequests
{
    /**
     * Validate search request input
     *
     * @param  Request $request
     * @throws Illuminate\Validation\ValidationException
     */
    protected function validate_search(Request $request)
    {
        
        $this->validate($request, [
            'gif'         => 'required|string|max:255'
          ],
          $this -> validation_messages()
        );
    }

    private function validation_messages()
    {
        return [
            'gif.required' => 'Please provide gif as a query string'
        ];
    }

}