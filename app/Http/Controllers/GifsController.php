<?php

namespace App\Http\Controllers;

use App\Http\Resources\GifResource;
use Illuminate\Http\Request;
use App\Http\Validators\ValidatesGifRequests;
use App\Http\Providers\GifProvider;

class GifsController extends Controller
{   
    use ValidatesGifRequests;

    public function get_random_gif(Request $request)
    {   
        $gif = GifProvider::random_gif();

        return response()->json($gif->as_resource());
    }

    public function search(Request $request)
    {   
        $this -> validate_search($request);
        
        $gif_name = $request->query('gif');

        $gif=GifProvider::search_gif($gif_name);

        if($gif === null){
            return response()->json(['error' => 'Resource not found'], 404);
        }
        
        return response()->json($gif->as_resource());
    }
}
