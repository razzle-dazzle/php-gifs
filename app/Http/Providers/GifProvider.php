<?php

namespace App\Http\Providers;

use App\Http\Resources\GifResource;

class GifProvider {

  private static $random_gifs = [
    "banana",
    "apple",
    "tomato"
  ];

  public static function pick_random_gif() {

    $random_gifs_count = count(self::$random_gifs);

    return self::$random_gifs[rand(0,$random_gifs_count-1)];
  }

  public static function random_gif(){


    $gif_name = self::pick_random_gif();

    return new GifResource($gif_name);
    
  }

  public static function search_gif($gif){

    $index = array_search($gif,self::$random_gifs);

    if($index === false){
      return;
    }
    
    return new GifResource(self::$random_gifs[$index]);

  }
}