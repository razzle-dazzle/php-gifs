<?php

class GifRandomTest extends TestCase
{
    // Integration tests
    /** @test */
    public function it_returns_unauthorized_random_gif_test()
    {
        $this->get('/v1/gifs/random');

        $this->seeJsonStructure([
                'error'
             ])
            ->seeStatusCode(403);
    }

    /** @test */
    public function it_returns_random_gif_test()
    {
        $response = $this->get('/v1/gifs/random', ['API_KEY' => 'apikey']);
        $response 
            ->seeStatusCode(200)
            ->seeJsonStructure([
                'data' => [
                    'gif' =>[
                        'title',
                        'url'
                    ]
                ],
             ]);
    }
}
