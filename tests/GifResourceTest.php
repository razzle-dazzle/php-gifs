<?php

use App\Http\Resources\GifResource;

class GifResourceTest extends TestCase
{
    // Unit tests
    /** @test */
    public function it_creates_a_gif_resource_test()
    {
        $gif_resource = new GifResource("strawberry");

        $this->assertSame($gif_resource->title , 'strawberry');
        $this->assertSame($gif_resource->slug , 'strawberry');
      
        $this->assertArrayHasKey('data',$gif_resource->as_resource());        
        
    }

}
