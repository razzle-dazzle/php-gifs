<?php

class GifSearchTest extends TestCase
{
    // Integration tests

    /** @test */
    public function it_returns_unauthorized_test()
    {
        $this->get('/v1/gifs/search');

        $this->seeJsonStructure([
                'error'
             ])
            ->seeStatusCode(403);
    }

    /** @test */
    public function it_returns_bad_request_when_validation_fails_test()
    {
        $response = $this->get('/v1/gifs/search', ['API_KEY' => 'apikey']);
        $response 
            ->seeStatusCode(422)
            ->seeJsonStructure([
                    'gif'
                ]);
    }

    /** @test */
    public function it_returns_gif_json_resource_test()
    {
        $response = $this->get('/v1/gifs/search?gif=banana', ['API_KEY' => 'apikey']);
        $response 
            ->seeStatusCode(200)
            ->seeJsonStructure([
                'data' => [
                    'gif' =>[
                        'title',
                        'url'
                    ]
                ],
             ]);
    }

    /** @test */
    public function it_returns_error_if_gif_not_found_test()
    {
        $response = $this->get('/v1/gifs/search?gif=unknowngif', ['API_KEY' => 'apikey']);
        $response 
            ->seeJsonStructure([
            'error'
            ])
            ->seeStatusCode(404);
    }
}
